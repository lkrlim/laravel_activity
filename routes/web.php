<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'PagesController@getIndex');

/*Route::get('user/{id}', function ($id) {
    echo "User: " . $id;
});

Route::get('user/{first}/{last}', function ($first, $last){
	echo "name: " . $first . " " . $last;
});

Route::get('user/{name?}', function($name = 'John'){
	echo "name: " . $name;
});

Route::get('test/{id}/{name}', function ($id, $name){
	echo "id: " .  $id . " name: " . $name;
}) -> where(['id' => '[0-9]+', 'name' => '[a-z]+']);*/

Route::get('contact', 'PagesController@getContact');
Route::get('about', 'PagesController@getAbout');
Route::resource('items', 'ItemController');