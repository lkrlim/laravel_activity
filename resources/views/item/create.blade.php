@extends('pages.main')

@section('title', ' | Create Item')

@section('stylesheet')
	{!! Html::style('css/parsley.css') !!}
@stop

@section('body')
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1>Create New Item</h1>
			<br>
			{!! Form::open(['route' => 'items.store', 'data-parsley-validate' => '']) !!}
			    {!! Form::label('itemname', 'Item name') !!}
			    {!! Form::text('itemname', null, ['class' => 'form-control', 'required' => '']) !!}
			    {!! Form::label('description', 'Description') !!}
			    {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => '']) !!}
			    {!! Form::label('quantity', 'Quantity') !!}
			    {!! Form::text('quantity', null, ['class' => 'form-control', 'required' => '', 'type' => 'number']) !!}
			    {!! Form::submit('Submit', array('class' => 'btn btn-success', 'style' => 'margin-top: 20px')) !!}
			{!! Form::close() !!}
		</div>
	</div>
@stop

@section('script')
	{!! Html::script('js/parsley.min.js') !!}
@stop