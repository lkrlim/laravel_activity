<!DOCTYPE html>
<html lang="en">
<head>
  @include('partials._head')
</head>
  <body>
    @include('pages.navbar')

    @yield('body')

    <div class="row">
      @include('partials._copyright')
    </div>
    
    @include('partials._scripts')
  </body>
</html>