@extends('pages.main')

@section('title', ' | About')

@section('body')
	<div class="container">
		<h1>About</h1>
		<br>
		<h2>Webmasters</h2>

		@if(count($webmasters))
			<ul>
				@foreach($webmasters as $webmaster)
					<li>{{ $webmaster }}</li>
				@endforeach
			</ul>
		@endif
	</div>
@endsection