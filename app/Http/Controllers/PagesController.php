<?php

namespace App\Http\Controllers;

class PagesController extends Controller{
	
	public function getIndex(){
		return view('pages.child');
	}

	public function getContact(){

		$first = 'Lianne';
		$last = "Lim";

		$fullname = $first. " " . $last;
		return view('pages.contact') ->with("full_name", $fullname);
	}

	public function getAbout(){

		$webmasters = ['John', 'Peter', 'Mark'];

		return view('pages.about', compact('webmasters'));
	}
}